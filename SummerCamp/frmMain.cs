﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SummerCamp
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
            lblStatus.Text = "Loaded successfully";
        }

        private void tsbViewChildren_Click(object sender, EventArgs e)
        {
            viewChildren();
        }

        private void viewChildren()
        {
            frmViewChildren frmNewChildren = new frmViewChildren();
            frmNewChildren.MdiParent = this;
            frmNewChildren.Show();
        }
        private void viewInventory()
        {
            frmViewInventory frmViewInventory = new frmViewInventory();
            frmViewInventory.MdiParent = this;
            frmViewInventory.Show();
        }

        private void tsbViewInventory_Click(object sender, EventArgs e)
        {
            viewInventory();
        }
    }
}
