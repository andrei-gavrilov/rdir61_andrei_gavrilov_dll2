﻿namespace SummerCamp
{
    partial class frmViewChildren
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.gbControls = new System.Windows.Forms.GroupBox();
            this.btnAddChildren = new System.Windows.Forms.Button();
            this.btnEditChildren = new System.Windows.Forms.Button();
            this.btnDeleteChildren = new System.Windows.Forms.Button();
            this.btnSaveChildren = new System.Windows.Forms.Button();
            this.rtbSearch = new System.Windows.Forms.RichTextBox();
            this.btnSearch = new System.Windows.Forms.Button();
            this.Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Firstname = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Lastname = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.gbControls.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToOrderColumns = true;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Id,
            this.Firstname,
            this.Lastname});
            this.dataGridView1.Location = new System.Drawing.Point(13, 120);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(813, 285);
            this.dataGridView1.TabIndex = 0;
            // 
            // gbControls
            // 
            this.gbControls.Controls.Add(this.rtbSearch);
            this.gbControls.Controls.Add(this.btnSearch);
            this.gbControls.Controls.Add(this.btnSaveChildren);
            this.gbControls.Controls.Add(this.btnDeleteChildren);
            this.gbControls.Controls.Add(this.btnEditChildren);
            this.gbControls.Controls.Add(this.btnAddChildren);
            this.gbControls.Location = new System.Drawing.Point(13, 13);
            this.gbControls.Name = "gbControls";
            this.gbControls.Size = new System.Drawing.Size(813, 101);
            this.gbControls.TabIndex = 1;
            this.gbControls.TabStop = false;
            this.gbControls.Text = "Actions";
            // 
            // btnAddChildren
            // 
            this.btnAddChildren.Location = new System.Drawing.Point(7, 19);
            this.btnAddChildren.Name = "btnAddChildren";
            this.btnAddChildren.Size = new System.Drawing.Size(75, 21);
            this.btnAddChildren.TabIndex = 0;
            this.btnAddChildren.Text = "Add Children";
            this.btnAddChildren.UseVisualStyleBackColor = true;
            // 
            // btnEditChildren
            // 
            this.btnEditChildren.Location = new System.Drawing.Point(88, 19);
            this.btnEditChildren.Name = "btnEditChildren";
            this.btnEditChildren.Size = new System.Drawing.Size(75, 21);
            this.btnEditChildren.TabIndex = 0;
            this.btnEditChildren.Text = "Edit Children";
            this.btnEditChildren.UseVisualStyleBackColor = true;
            this.btnEditChildren.Click += new System.EventHandler(this.button3_Click);
            // 
            // btnDeleteChildren
            // 
            this.btnDeleteChildren.Location = new System.Drawing.Point(169, 19);
            this.btnDeleteChildren.Name = "btnDeleteChildren";
            this.btnDeleteChildren.Size = new System.Drawing.Size(75, 21);
            this.btnDeleteChildren.TabIndex = 0;
            this.btnDeleteChildren.Text = "Delete Children";
            this.btnDeleteChildren.UseVisualStyleBackColor = true;
            this.btnDeleteChildren.Click += new System.EventHandler(this.button3_Click);
            // 
            // btnSaveChildren
            // 
            this.btnSaveChildren.Location = new System.Drawing.Point(250, 19);
            this.btnSaveChildren.Name = "btnSaveChildren";
            this.btnSaveChildren.Size = new System.Drawing.Size(75, 21);
            this.btnSaveChildren.TabIndex = 0;
            this.btnSaveChildren.Text = "Save";
            this.btnSaveChildren.UseVisualStyleBackColor = true;
            this.btnSaveChildren.Click += new System.EventHandler(this.button3_Click);
            // 
            // rtbSearch
            // 
            this.rtbSearch.Location = new System.Drawing.Point(7, 50);
            this.rtbSearch.Name = "rtbSearch";
            this.rtbSearch.Size = new System.Drawing.Size(237, 21);
            this.rtbSearch.TabIndex = 1;
            this.rtbSearch.Text = "";
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(250, 50);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(75, 21);
            this.btnSearch.TabIndex = 0;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.button3_Click);
            // 
            // Id
            // 
            this.Id.HeaderText = "Id";
            this.Id.Name = "Id";
            this.Id.ReadOnly = true;
            this.Id.Width = 45;
            // 
            // Firstname
            // 
            this.Firstname.HeaderText = "Firstname";
            this.Firstname.Name = "Firstname";
            this.Firstname.ReadOnly = true;
            // 
            // Lastname
            // 
            this.Lastname.HeaderText = "Lastname";
            this.Lastname.Name = "Lastname";
            this.Lastname.ReadOnly = true;
            // 
            // frmViewChildren
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(838, 417);
            this.Controls.Add(this.gbControls);
            this.Controls.Add(this.dataGridView1);
            this.Name = "frmViewChildren";
            this.Text = "View Children";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.gbControls.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.GroupBox gbControls;
        private System.Windows.Forms.Button btnEditChildren;
        private System.Windows.Forms.Button btnAddChildren;
        private System.Windows.Forms.Button btnSaveChildren;
        private System.Windows.Forms.Button btnDeleteChildren;
        private System.Windows.Forms.RichTextBox rtbSearch;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn Firstname;
        private System.Windows.Forms.DataGridViewTextBoxColumn Lastname;
    }
}